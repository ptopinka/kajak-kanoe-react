import React from "react";


import { Layout } from "antd";
import Router from './Router';
import "./App.css";

const App = () => {


  return (
    <Layout className="layout">
        <Router/>
    </Layout>
  );
};

export default App;
