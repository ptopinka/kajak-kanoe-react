import React from "react";
import { Col, Layout, Row } from "antd";
import "./Contacts.css";
import * as Constants from "../../constants";
import { Link } from "react-router-dom";
//import {Icon }from 'antd'

const Contacts = () => {
  const { Content } = Layout;
  const WrapperComponent = "a";

  return (
    <Content style={{ padding: "0 0px" }}>
      <div style={{ background: "#fff", padding: 24, minHeight: 280 }}>


        <Row>
          <Col span={6}>
            <h3 style={{color: 'black'}}> Pádla Braca</h3>
          </Col>

          <Col span={6}>
            <div style={{textAlign: 'left'}}>
              <div>Firma:<br/>
                <p style={{textAlign: 'left'}}>
                  {/*<span><Icon type="contacts" />&nbsp;Michal Pfoff</span><br/>*/}
                  <span>TNK Servis s.r.o</span><br/>
                  <span>U loděnice 966, 156 00 Praha 5 Zbraslav</span><br/>
                  {/*<span><Icon type="mail" />&nbsp;info@kajak-kanoe.cz</span><br/>*/}
                  {/*<span><Icon type="phone" />&nbsp;725 93 59 61</span><br/>*/}
                </p>
              </div>
            </div>
          </Col>
          <Col span={6}>
            <Link to={"/paddles"}>
              <img
                  className="contacts_main_image"
                  src={`${Constants.IMAGES_PATH}/braca_logo.png`}
                  alt="1X"
              />
            </Link>
          </Col>
        </Row>
        <Row>
          <Col span={24}>
          </Col>
        </Row>


        <Row>
          <Col span={6}>
              <h3 style={{color: 'black'}}>Lodě Nelo</h3>
          </Col>
          <Col span={6}>
            <div style={{textAlign: 'left'}}>
              <div>
                <p style={{textAlign: 'left'}}>
                  <span>&nbsp;Petr Mokrý</span><br/>
                  <span>TNK Servis s.r.o</span><br/>
                  <span>U loděnice 966, 156 00 Praha 5 Zbraslav</span><br/>
                  <span>&nbsp;petr.mokry@kajak-kanoe.cz</span><br/>
                  <span>&nbsp;604 23 62 69</span><br/>
                </p>

              </div>
            </div>
          </Col>
          <Col span={6}>
            <Link to={"/boats"}>
              <img
                  className="contacts_main_image"
                  src={`${Constants.IMAGES_PATH}/nelo_logo.png`}
                  alt="1X"
              />
            </Link>
          </Col>
        </Row>
        <Row>
          <Col span={24}>
          </Col>
        </Row>
        <Row>
          <Col span={6}>

            <p>
              <h3 style={{color: 'black'}}> Testovací centrum Nelo Rowing:</h3>
            </p>
          </Col>
          <Col span={6}>
            <p style={{textAlign: 'left'}}>
              <span>&nbsp;Pavel Topinka</span><br/>
              <span>TNK Servis s.r.o</span><br/>
              <span>Loděnice USK Praha</span><br/>
              <span>Strakonická 49, 159 00 Malá Chuchle</span><br/>
              <span>&nbsp;777 33 10 80</span><br/>
            </p>


          </Col>
          <Col span={6}>

            <div style={{ background: "black", height: "100px", width: "300px" }}>
              <a href="http://wwww.nelorowing.cz">
                <img
                    className="contacts_main_image"
                    src={`${Constants.IMAGES_PATH}/nelo_rowing_logo.png`}
                    alt="1X"
                />
              </a>
            </div>

          </Col>

        </Row>
        <Row>
          <Col span={24}>
          </Col>
        </Row>


        <Row>
          <Col span={6}>
            <h3 style={{color: 'black'}}> Sídlo firmy</h3>
          </Col>

          <Col span={6}>
            <div style={{textAlign: 'left'}}>
              <div>Firma:<br/>
                <p style={{textAlign: 'left'}}>
                  <span>&nbsp;Pavel Topinka</span><br/>
                  <span>TNK Servis s.r.o</span><br/>
                  <span>U loděnice 966, 156 00 Praha 5 Zbraslav</span><br/>
                  <span>&nbsp;ptopinka@kajak-kanoe.cz</span><br/>
                  <span>&nbsp;777 33 10 80</span><br/>
                </p>
              </div>
            </div>
          </Col>

        </Row>


      </div>
    </Content>
  );
};
export default Contacts;
