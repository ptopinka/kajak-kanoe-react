import {Drawer, Layout, Menu, Row, Col} from "antd";
import {Link} from "react-router-dom";
import React, {useState} from "react";
import styles from './Header.css';
import * as Constants from '../constants'
import {MenuOutlined} from "@ant-design/icons"


const Header = () => {
    const {Header} = Layout;
    const [openMenu, setOpenMenu] = useState(false)
    return (
        <Header class="appHeader">

            <span class="headerMenu">
            <DesktopHeader/>
            </span>
            <span class="menuIcon">
            <MobileHeader/>

            </span>

            <Row type="flex">
                <Col span={24}>
                    <div style={{
                        backgroundColor: "darkblue",
                        height: 60,
                        paddingLeft: 12,
                        paddingTop: 12,
                        width: "100%"
                    }}
                         onClick={() => {
                             setOpenMenu(true)
                         }}
                         class="menuIcon"
                    >
                        <MenuOutlined style={{color: "white", fontSize: "xx-large"}}/>
                    </div>
                    <span class="headerMenu">
                        <AppMenu/>
                    </span>
                    {<Drawer open={openMenu}
                             closable={false}
                             placement="left"
                             onClose={() => {
                                 setOpenMenu(false);
                             }}
                             bodyStyle={{backgroundColor: "darkblue"}}
                             width={290}

                    >
                        <AppMenu isInline/>
                    </Drawer>}

                </Col>


            </Row>


        </Header>
    )
}

const DesktopHeader = () => {
    return (
        <Row class="appHeader_rowLogo">
            <Col span={24} class="appHeader__logo">
                <div style={{display: "inline"}}>
                    <div style={{float: "left", padding: "70px 0 0 30px"}}>
                        <Link to="/home"><img style={{width: 350}} src={`${Constants.IMAGES_PATH}logo.PNG`}
                                              alt="Logo"/></Link>
                    </div>
                    <div style={{overflow: "hidden"}}>
                        <img src={`${Constants.IMAGES_PATH}header_right_cz.png`} alt="Logo"
                             class="appHeader__logo_right"/>
                    </div>
                </div>
            </Col>
        </Row>

    )
}

const MobileHeader = () => {
    return (
        <Row class="appHeader_rowLogo ">
            <Col span={24} class="appHeader__logo">

                <div class="menuIcon" style={{display: "inline"}}>
                    <div style={{float: "left", padding: "35px 10px 0px 10px"}}>
                        <Link to="/home"><img style={{width: 120}} src={`${Constants.IMAGES_PATH}logo.PNG`}
                                              alt="Logo"/></Link>
                    </div>
                    <div style={{overflow: "hidden"}}>
                        <img src={`${Constants.IMAGES_PATH}header_right_mobile_cz.png`} alt="Logo"
                             class="appHeader__logo_right_mobile"/>
                    </div>
                </div>
            </Col>
        </Row>

    )
}

function AppMenu({isInline = false}) {
    return (
        <Menu

            mode={isInline ? "inline" : "horizontal"}

            style={{
                backgroundColor: "darkblue",
                color: "white",
                fontSize: "xx-large",
                border: "none",
                paddingTop: 25,

            }}


        >
            <Menu.Item className="menu" key="1"><Link to="/paddles">Braća</Link></Menu.Item>
            <Menu.Item className="menu" key="2"><Link to="/boats">Nelo</Link></Menu.Item>
            <Menu.Item className="menu" key="3"><Link to="/parts">Vybavení</Link></Menu.Item>
            <Menu.Item className="menu" key="4"><Link to="/media">Media</Link></Menu.Item>
            {/*<Menu.Item className="menu" key="2"><Link to="/design">Design</Link></Menu.Item>*/}
            <Menu.Item className="menu" key="6"><Link to="/contact">Kontakty</Link></Menu.Item>

        </Menu>
    )
}

export default Header;

