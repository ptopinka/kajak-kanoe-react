import {Layout, Row, Col} from "antd";
import React from "react";
//import {Icon }from 'antd'

const Footer = () => {
    const { Footer } = Layout;
    return (
        <Footer style={{ textAlign: "center", background: "darkblue", color:"white" }}>

{/*            <Row>
                <Col span={6} style={{textAlign: 'left'}}>
                    <h3 style={{color: 'white'}}>O nás</h3>
                    <p style={{textAlign: 'justify'}}>
                        V roce 2019 se firma TNK servis stala oficíálním distributorem Nelo Rowing pro Českou Republiku.
                        Díky naším dlouholeté spolupráci s firmou Nelo při prodeji jejich kajaků, které patří ke světové
                        spičce jsme se dohodli, že budeme prodávat i veslařské lodě.</p>
                    <p>Firma TNK úzce spolupracuje s
                        Mirkou Topinkovou Knapkovou a trenérem Tomášem Kacovským.
                    </p>
                </Col>
                <Col span={4}></Col>
                <Col span={4} style={{textAlign: 'left'}}><h3 style={{color: 'white'}}>Kontakty</h3>
                    <div style={{textAlign: 'left'}}>
                        <div>Firma:<br/>
                            <p style={{textAlign: 'left'}}>
                                <span>&nbsp;Pavel Topinka</span><br/>
                                <span>TNK Servis s.r.o</span><br/>
                                <span>U loděnice 966, 156 00 Praha 5 Zbraslav</span><br/>
                                <span>&nbsp;ptopinka@kajak-kanoe.cz</span><br/>
                                <span>&nbsp;777 33 10 80</span><br/>
                            </p>

                        </div>



                    </div>
                </Col>
                <Col span={4} style={{textAlign: 'left'}}>
                    <h3 style={{color: 'white'}}>Lodě Nelo</h3>
                    <div style={{textAlign: 'left'}}>
                        <div>
                            <p style={{textAlign: 'left'}}>
                                <span>&nbsp;Petr Mokrý</span><br/>
                                <span>Loděnice Edy Fulína</span><br/>
                                <span>U loděnice 966, 156 00 Praha 5 Zbraslav</span><br/>
                                <span>&nbsp;petr.mokry@kajak-kanoe.cz</span><br/>
                                <span>&nbsp;604 23 62 69</span><br/>
                            </p>

                        </div>
                    </div>
                </Col>

                <Col span={6} style={{textAlign: 'left'}}>
                    <h3 style={{color: 'white'}}> Testovací centrum Nelo Rowing:</h3>

                    <p style={{textAlign: 'left'}}>
                        <span><Icon type="contacts" />&nbsp;Pavel Topinka</span><br/>
                        <span>Loděnice USK Praha</span><br/>
                        <span>Strakonická 49, 159 00 Malá Chuchle</span><br/>
                        <span>&nbsp;777 33 10 80</span><br/>
                    </p>
                </Col>
            </Row>*/}
            <Row>
                <Col span={24}>&nbsp;</Col>
            </Row>
            <Row>
                <Col span={24}>Pavel Topinka ©2020 TNK Servis s.r.o.</Col>
            </Row>
        </Footer>
    )
}
export default Footer;