import React from "react";
import {Col, Layout, Row} from "antd";
import "./Home.css";
import * as Constants from "../../constants";
import {Link} from "react-router-dom";

const Home = () => {
    const {Content} = Layout;
    const WrapperComponent = "a";

    return (
        <Content style={{padding: "0 0px"}}>
            <div style={{background: "#fff", padding: "50px 150px 50px 150px", minHeight: "600px"}}>
                <Row>
                    <Col span={24}>
                        <h3>
                            Firma TNK Servis se specializuje na dovoz a prodej věcí pro
                            rychlostní kanoistiku a veslování.
                        </h3>
                    </Col>
                </Row>
                <Row>
                    <Col span={24}>
                        &nbsp;
                    </Col>
                </Row>
                <Row>
                    <Col span={24}>
                        &nbsp;
                    </Col>
                </Row>
                <Row>
                    <Col span={12}>
                        <p class="brand_header">
                            Braća Sport
                        </p>
                        <p>
                            Od roku 2016 jsme výhradní dovozce pádel a oblečeni Braca do České Republiky.
                        </p>
                    </Col>
                    <Col span={12}>
                        <Link to={"/paddles"}>
                            <img
                                className="home_main_image"
                                src={`${Constants.IMAGES_PATH}/braca_logo.png`}
                                alt="1X"
                            />
                        </Link>
                    </Col>
                </Row>
                <Row>
                    <Col span={24}>
                    </Col>
                </Row>


                <Row>
                    <Col span={12}>
                        <p class="brand_header">
                            Nelo
                        </p>

                        <p>
                            Od roku 2020 jsme výhradní dovozce lodí Nelo do České
                            Republiky.
                        </p>
                    </Col>
                    <Col span={12}>
                        <Link to={"/boats"}>
                            <img
                                className="home_main_image"
                                src={`${Constants.IMAGES_PATH}/nelo_logo.png`}
                                alt="1X"
                            />
                        </Link>
                    </Col>
                </Row>
                <Row>
                    <Col span={24}>
                    </Col>
                </Row>
                <Row>
                    <Col span={12}>
                        <p class="brand_header">
                            Nelo Rowing
                        </p>
                        <p>
                            Od roku 2019 jsme výhradní dovozce lodí Nelo Rowing do České
                            Republiky.
                        </p>
                    </Col>
                    <Col span={12}>
                        <div style={{background: "white"}}>
                            <a href="http://wwww.nelorowing.cz">
                                <img
                                    className="home_main_image"
                                    src={`${Constants.IMAGES_PATH}/nelo_rowing_logo.png`}
                                    alt="1X"
                                />
                            </a>
                        </div>
                    </Col>
                </Row>


            </div>
        </Content>
    );
};
export default Home;
