import React from "react";

import { Layout } from "antd";
import Header from "./../Header";
import Footer from "./../Footer";

const Media = () => {
  const { Content } = Layout;
  return (
    <>
      <Content style={{ padding: "0 50px" }}>
        <div style={{ background: "#fff", padding: 24, minHeight: 480 }}>
          <h1>Nelo design trupu s plochým dnem může být opravdu rychlý</h1>
          <iframe
            width="560"
            height="315"
            src="https://www.youtube.com/embed/P5g7eJ-mVBQ"
            frameborder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowfullscreen
          />
          <h1> Srovnání skifů Nelo a Filippi</h1>
          <iframe
            width="560"
            height="315"
            src="https://www.youtube.com/embed/v6EpdnX_FLE"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen
          />
        </div>
      </Content>
    </>
  );
};
export default Media;
