import React from "react";

import { Layout, Row, Col } from "antd";
import Header from "./../Header";
import Footer from "./../Footer";
import * as Constants from "../../constants";

const Technology = () => {
  const { Content } = Layout;
  return (
      <Content style={{ padding: "0 50px" }}>
        <Row>
          <Col span={4}>
            <img
              className="responsive_image"
              src={`${Constants.IMAGES_PATH}/reremoskif/proa.png`}
              alt="1X"
            />
          </Col>
          <Col span={16}>
            <div style={{ background: "#fff", padding: 24, minHeight: 480 }}>
              <h1>Technologie a Inovace</h1>
              <div>
                Lodě Nelo jsou konstruovány z karbonu a prostřední vrstva je z
                PVC. Tyto materiály poskytují větší tuhost a trvanlivost.
                Protože používáme dvojitou formu, tak vznikají bezproblémové a
                vynikající povrchové úpravy.
              </div>
              <div>
                <img
                  className="responsive_image"
                  src={`${Constants.IMAGES_PATH}/reremoskif/reflexo.jpg`}
                  alt="1X"
                />
              </div>
              <div>
                Křidla si vyrábíme sami z karbonu s vekým důrazem na pevnost a
                trvanlivost a jednoduché nastavení. Mohou být s přiznaným
                karbonem nebo v barevném provedení.
              </div>
              <br />
              <div>
                Náš konstrukční team vyvinul loď s trupem, který je je plošší s
                hlavním úkolem zajistit aby ve vodě klouzal déle a výše a
                zredukoval houpavý pohyb lodi při každém tempu. Plošší dno
                graduje na přídi, která je převrácená. Snižuje tak odpor a
                zachovává všechny výhody.
              </div>
              <div>
                <img
                  className="responsive_image"
                  src={`${Constants.IMAGES_PATH}/reremoskif/calhas.png`}
                  alt="1X"
                />
              </div>
              <div>
                Naši tým inženýrů také tvrdě pracoval na lepším řešení pro
                uchycení a vybavení lodi což posunuje představu co existuje a co
                lze vylepšit ve veslování.
                <br />
                Vyvinuli jsme nový koncept slajdu a kolejniček. Sezení je velmi
                tuhé a lehké a zachováná komfort sezení. Kolejničky využívají
                celou délku která je k dispozici v lodi a nelimituje veslaře v
                žádném směru a tak není potřeba loď upravovat. Obrácený profil
                umožňuje udržet kolejničky stále čisté.
              </div>{" "}
            </div>
          </Col>
          <Col span={4}>
            {" "}
            <img
              className="responsive_image"
              src={`${Constants.IMAGES_PATH}/reremoskif/aranha2.png`}
              alt="1X"
            />
          </Col>
        </Row>
      </Content>

  );
};
export default Technology;
