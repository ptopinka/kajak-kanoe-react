import React from "react";
import * as Constants from "../../constants";
import { useState } from "react";
import {Col, Drawer, Layout, Row} from "antd";

import "./Paddles.css";
import PaddleDescription from "./PaddleDescription";
import PaddleDescriptionMobile from "./PaddleDescriptionMobile";
import ClothingDescription from "./ClothingDescription";
import AccessoriesDescription from "./AccessoriesDescription";
import {accesories} from "./accessories";
import {paddles_db} from "./paddles_db";
import {MenuOutlined} from "@ant-design/icons";
import useWindowDimensions from "../hooks/getDimensionHook";
const Paddles = () => {
  const { Content } = Layout;
  const WrapperComponent = "a";


  const [selectedPaddle, setSelectedPaddle] = useState(paddles_db[0]);
    const [openMenu, setOpenMenu] = useState(false)

  const Paddle = (paddle) => {
    return (
      <a
        href={"#"}
        onClick={(e) => {
          e.preventDefault();
          setSelectedPaddle(paddle);
        }}
      >
        {" "}
        {paddle.name}
      </a>
    );
  };

  const Paddles = (paddles_db, type) => {
    return (
      <ul>
        {paddles_db.map((paddle, index) => {
          if (paddle.type === type) {
            return (
              <li>
                <Paddle {...paddle} />
              </li>
            );
          }
        })}
      </ul>
    );
  };

  const Accessories = (accessories, type) => {
    return (
        <ul>
          {accessories.map((accessories, index) => {
            if (accessories.type === type) {
              return (
                  <li>
                    <Paddle {...accessories} />
                  </li>
              );
            }
          })}
        </ul>
    );
  };

  const LeftMenu = () => {
   return(
       <>
         Kanoistické pádla:
         {Paddles(paddles_db, "canoe")}
         Kajakářské pádla:
         {Paddles(paddles_db, "kayak")}

         Oblečení
         {Accessories(accesories,"clothing" )}
         Vybavení
         {Accessories(accesories,"accessories" )}
       </>
       );

  }

    const { height, width } = useWindowDimensions();
    let colSpan = (width < 800 ? 2 : 6);
    let colSpanBody = (width < 800 ? 22 : 18);
    console.debug("..............", height, width);
  //let  spanCol = (width < 800)

  return (
    <Content style={{ padding: "0 0px" }}>
      <div style={{ background: "#fff", padding: 24, minHeight: 280 }}>
        <Row>
          <Col span={colSpan}>
              <div style={{backgroundColor: "darkblue", height: 35, paddingLeft: 9, width: 35, paddingTop: 9}}
                   onClick={() => {
                       setOpenMenu(true)
                   }}
                   className="menuIcon"
              >
                  <MenuOutlined style={{color: "white", fontSize: "medium"}}/>
              </div>
            <span class="headerMenu">
                <LeftMenu/>
            </span>
              {<Drawer open={openMenu}
                       closable={false}
                       placement="left"
                       onClose={() => {
                           setOpenMenu(false);
                       }}
                       bodyStyle={{backgroundColor: "darkblue"}}
                       width={290}

              >
                  <LeftMenu />
              </Drawer>}
          </Col>
          <Col span={colSpanBody}>
            { (selectedPaddle.type === 'kayak' || selectedPaddle.type === 'canoe') && width < 800 &&  <PaddleDescriptionMobile {...selectedPaddle} />}
            { (selectedPaddle.type === 'kayak' || selectedPaddle.type === 'canoe') && width > 800 &&  <PaddleDescription {...selectedPaddle} />}
            { selectedPaddle.type === 'clothing' &&  <ClothingDescription {...selectedPaddle} />}
            { selectedPaddle.type === 'accessories' &&  <AccessoriesDescription {...selectedPaddle} />}


          </Col>
        </Row>
        <Row>
          <Col span={24}></Col>
        </Row>

        <Row>
          <Col span={24}>&nbsp;</Col>
        </Row>

        <Row>
          <Col key="1" span={12}>
            kompletní nabídka v{" "}
            <WrapperComponent
              href={`https://paddles.braca-sport.com/paddles/kayak-flatwater/braca-i.html`}
              target="_empty"
              title="braca-sport.com"
            >
              Braca-Sport.com
            </WrapperComponent>
          </Col>
          <Col key={"2"} span={12}>
            * Cena zahrnuje DPH
          </Col>
        </Row>
      </div>
    </Content>
  );
};



export default Paddles;
