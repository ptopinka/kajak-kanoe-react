export   const accesories = [
    {
        type: "clothing",
        name: "Brača Performance Thermal Shirt",
        picture_name: 'performance_shirt_colors.png',
        adjustment: "plastic",
        price: [
            {
                name: "Krátký rukáv",
                price: "1.080 Kč",
            },
            { name: "dlouhý rukáv", price: "+ 1,235.00 Kč" }
        ],
        sizes: ['XS','S','M', 'L','XL'],
        colours: ["žlutá", "růžová", "bílá", "černá"],

        url:
            "https://paddles.braca-sport.com/accessories/performance-shirt.html",

        description:
            "Je navrženo a vyrobeno pro náročné tréninky a závody.  " +
            "Nabízí unikátní rovnováhu mezi udržováním tepla a vzdušnosti a skvěle odvádí pot od těla."
            },
    {
        type: "clothing",
        name: "Brača kšiltovka",
        picture_name: 'cap.png',
        adjustment: "plastic",
        price: [
            {
                name: "Braća-Sport Cap",
                price: "463 Kč",
            }
        ],
        sizes: ['one size'],
        colours: [ "bílá", "černá"],

        url:
            "https://paddles.braca-sport.com/accessories/cap.html",

        description:
            "skvěle sedne a je bezvedná pro každodenní nošení i sport."
    },

    {
        type: "accessories",
        name: "obal na padlo ",
        adjustment: "plastic",
        picture_name: "paddle_cover_kayak.png",
        price: [
            {
                name: "Kajak",
                price: "1,698 Kč",
            },
            { name: "Kanoe", price: "1,698.00 Kč" },
        ],
        sizes: [],

        url:
            "https://paddles.braca-sport.com/accessories/paddle-covers/braca-kayak-flatwater-covers.html",

        description:
            "Ochraňuje pádlo při trasportu nebo zrovna když s ním nejsi na vodě. "


    },
    {
        type: "accessories",
        name: "špricdeka",
        adjustment: "",
        picture_name: "spray_skirt_pro_colors.png",
        price: [
            {
                name: "Kajak",
                price: "1,698 Kč",
            },

        ],
        sizes: ["S/M", "L/XL"],
        colours: ["bílá", "černá", "červená", "modrá"],

        url:
            "https://paddles.braca-sport.com/accessories/spray-skirt.html",

        description:
            "Je navržená k aby kokpit lodi zůstal suchý při tréninku a závodech za jakéhokoliv počasí " +
            "má posunovací pás z neoprenu a suchého zipu.\n" +
            "vysoká vodotěsnost. "


    }
,
    {
        type: "accessories",
        name: "rukavice classic",
        adjustment: "",
        picture_name: "pogies_kayak_2.png",
        price: [
            {
                name: "Kajak",
                price: "618 Kč",
            },

        ],
        sizes: [""],
        colours: [ "černá", "červená", "modrá"],

        url:
            "https://paddles.braca-sport.com/accessories/pogies.html",

        description:
            "rukavice pro zimni treninky. Ochrana pro vodě a větru. "


    }

    ,
    {
        type: "accessories",
        name: "rukavice marathon",
        adjustment: "",
        picture_name: "pogies_kayak_marathon_bg.png",
        price: [
            {
                name: "maraton",
                price: "618 Kč",
            },

        ],
        sizes: [""],
        colours: [ "černá"],

        url:
            "https://paddles.braca-sport.com/accessories/pogies.html",

        description:
            "rukavice pro zimni treninky. Ochrana pro vodě a větru. "


    }
    ,
    {
        type: "accessories",
        name: "rukavice canoe",
        adjustment: "",
        picture_name: "pogies_canoe.png",
        price: [
            {
                name: "canoe",
                price: "618 Kč",
            },

        ],
        sizes: [""],
        colours: [ "černá"],

        url:
            "https://paddles.braca-sport.com/accessories/pogies.html",

        description:
            "rukavice pro zimni treninky. Ochrana pro vodě a větru. "


    }    ,
    {
        type: "accessories",
        name: "kanoe trhačka",
        adjustment: "",
        picture_name: "canoe_wristband.png",
        price: [
            {
                name: "canoe",
                price: "463 Kč",
            },

        ],
        sizes: [""],
        colours: [ "černá"],

        url:
            "https://paddles.braca-sport.com/accessories/canoe-wristband.html",

        description:
            "Perfektní pomůcka pro kanoisty. Umožňuje připoutání spodní ruky k pádlu a snižuje tak nápor na zápěstí. "


    }
];