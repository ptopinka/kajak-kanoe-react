import React from "react";
import {Col, Layout, Row, Table, Modal} from "antd";
import * as Constants from "../../constants";
import "./Paddles.css";

const PaddleDescription = ({
                               name,
                               price,
                               sizes,
                               description,
                               picture_name,
                               adjustment,
                               url,
                           }) => {
    const WrapperComponent = "a";
    const columns = [
        {
            title: "Name",
            dataIndex: "name",
            key: "name",
        },
        {
            title: "Cena *",
            dataIndex: "price",
            key: "price",
        },
    ];
    const paginationProps = {
        hideOnSinglePage: true,
    };

    const info = () => {
        Modal.info({
            width: 1000,
            title: '',
            content: (
                <div>
                    <img
                        className="image_system"
                        src={`${Constants.IMAGES_PATH}/padla/big/${picture_name}`}
                        alt="1X"
                    />
                </div>
            ),
            onOk() {
            },
        });
    }

    return (
        <>
            <Row>
                <Col span={22}>
                    {" "}
                    <img
                        className="image_paddle"
                        src={`${Constants.IMAGES_PATH}/padla/${picture_name}`}
                        alt="1X"
                        onClick={info}
                    />

                </Col>
            </Row>
            <Row gutter={[16, 16]}>
                <Col span={22}>
                    <h2> {name}</h2>
                    <div>
                        <b>{name}</b>&nbsp;
                        {description}
                    </div>
                    <Row><Col span={24}>&nbsp;</Col> </Row>
                    <ul>
                        <li>
                            <span>velikosti: {sizes.join(", ")} cm&sup2;</span>
                        </li>

                        <li>
                            <span>Cena </span>
                            <Table
                                columns={columns}
                                dataSource={price}
                                bordered
                                pagination={{...paginationProps}}
                            />
                        </li>
                    </ul>

                    <div>
                        <br/>
                        originální popis pádla včetně rozměrů a technické specifikace{" "}
                        <WrapperComponent href={url} target="_empty" title="PDF">
                            {name}
                        </WrapperComponent>
                    </div>
                </Col>

            </Row>
            <Row>
                <Col span={22}>
                    <br/>
                    <br/>
                    <br/>
                    <h3>Rozkládací systém</h3>
                    <img
                        className="image_system"
                        src={`${Constants.IMAGES_PATH}/padla/braca_adj_system_${adjustment}.png`}
                        alt="1X"
                    />
                </Col>
            </Row>
        </>
    );
};
export default PaddleDescription;
