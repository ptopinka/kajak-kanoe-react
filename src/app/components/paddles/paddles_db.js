export  const paddles_db = [
    {
        type: "kayak",
        name: "Braca Kid",
        adjustment: "plastic",
        picture_name: "braca_kid.png",
        price: [
            {
                name: "Classic",
                price: "5,670 Kč",
            },
            { name: "Plastová spojka", price: "+ 1,073.00 Kč" },
            { name: "obal", price: "+ 1,575.00 Kč" },
        ],
        sizes: [580, 615, 655],

        url:
            "https://paddles.braca-sport.com/paddles/kayak-flatwater/braca-kid.html",

        description:
            "Jedná se o pádlo s paralelnímy hranami a je speciálně uspúsobené dětskému záběru.  " +
            "Tvar a použité materiály umožňují absorbci nárazů každého záběru a redukuje rázy do ramen, loktů a zápěstí." +
            "Pro dětská pádla máme výborné zkušenosti s plastovými spojkami. Jsou velmi pevné a lehčí. Také nejsou tak náchylné " +
            "na zarůstání jako hliníkové. Děti běžně zapomínají pádlo pravidelně rozebírat.",
    },
    {
        type: "kayak",
        name: "Braca Childr" +
            "en",
        picture_name: "braca_children.png",
        adjustment: "plastic",
        price: [
            {
                name: "Classic",
                price: "5,670 Kč",
            },
            { name: "Plastová spojka", price: "+ 1,073.00 Kč" },
            { name: "obal", price: "+ 1,575.00 Kč" },
        ],
        sizes: [610, 635],

        url:
            "https://paddles.braca-sport.com/paddles/kayak-flatwater/braca-children.html",

        description:
            "Braca Children je přesná kopie Braca I a Braca IV listu a je proporčně zmenšeno.  " +
            "použité materiály umožňují absorbci nárazů každého záběru a redukuje rázy do ramen, loktů a zápěstí." +
            "Pro dětská pádla máme výborné zkušenosti s plastovými spojkami. Jsou velmi pevné a lehčí. Také nejsou tak náchylné" +
            " na zarůstání jako hliníkové. Děti běžně zapomínají pádlo pravidelně rozebírat.",
    },
    {
        type: "kayak",
        name: "Braca I",
        picture_name: "braca_i.png",
        adjustment: "metal",
        price: [
            {
                name: "Classic",
                price: "10,238.00 Kč",
            },
            { name: "Extra Light", price: "11,183.00 Kč" },
            { name: "Marathon", price: "10,558.00 Kč" },
            { name: "Kovová spojka", price: "+ 1,473.00 Kč" },
            { name: "obal", price: "+ 1,575.00 Kč" },
        ],
        sizes: [775, 790, 805, 820, 840],

        url:
            "https://paddles.braca-sport.com/paddles/kayak-flatwater/braca-i.html",

        description:
            "je původně první pádlo ve tvaru padajíci kapky a je to naše nejvíce používané pádlo " +
            "Pádlo je tvarováno k chycení vody na začátku a uprostřed záběru a tak umožňuje dosáhnout maxímální síly na začátku záběru",
    },
    {
        type: "kayak",
        name: "Braca II",
        picture_name: "braca_ii.png",
        adjustment: "metal",
        price: [
            {
                name: "Classic",
                price: "10,238.00 Kč",
            },
            { name: "Extra Light", price: "11,183.00 Kč" },
            { name: "Marathon", price: "10,558.00 Kč" },
            { name: "Kovová spojka", price: "+ 1,473.00 Kč" },
            { name: "obal", price: "+ 1,575.00 Kč" },
        ],
        sizes: [775, 790, 805, 820, 840],
        url:
            "https://paddles.braca-sport.com/paddles/kayak-flatwater/braca-ii.html",

        description:
            "is based on the Brača I design, it has increased blade pitch (see diagram below), which provides a firmer " +
            "catch and more effortless exit than Brača I.",
    },
    {
        type: "kayak",
        name: "Braca IV",
        picture_name: "braca_iv.png",
        adjustment: "metal",

        price: [
            {
                name: "Classic",
                price: "10,238.00 Kč",
            },
            { name: "Extra Light", price: "11,183.00 Kč" },
            { name: "Marathon", price: "10,558.00 Kč" },
            { name: "Soft", price: "10,238.00 Kč" },
            { name: "60% Carbon", price: "7,718.00 Kč" },
            { name: "Surf Ski matte surface", price: "10,558.00 Kč" },
            { name: "Kovová spojka", price: "+ 1,473.00 Kč" },
            { name: "obal", price: "+ 1,575.00 Kč" },
        ],
        sizes: [765,735,705,670,635,610],
        url:
            "https://paddles.braca-sport.com/paddles/kayak-flatwater/braca-iv.html",

        description:
            "Brača IV is the original world's first 'tear-drop' design and our most widely used paddle. The paddle is designed " +
            "to facilitate application of power in the beginning (catch) and in the middle of the stroke, enabling capabilities" +
            " to obtain the “ultimate” forward stroke.",
    },
    ,
    {
        type: "kayak",
        name: "Braca VIII",
        picture_name: "braca_viii.png",
        adjustment: "metal",
        price: [
            {
                name: "Classic",
                price: "10,238.00 Kč",
            },
            { name: "Kovová spojka", price: "+ 1,473.00 Kč" },
            { name: "obal", price: "+ 1,575.00 Kč" },
        ],
        sizes: [837,799,767],

        url:
            "https://paddles.braca-sport.com/paddles/kayak-flatwater/braca-viii.html",

        description:
            "Brača VIII is based on the latest innovative blade concept. It was designed and engineered to facilitate a " +
            "softer catch and generation of smooth, precise and consistent strokes",
    },
    {
        type: "kayak",
        name: "Braca XI",
        picture_name: "braca_xi.png",
        adjustment: "metal",
        price: [
            {
                name: "Classic",
                price: "10,238.00 Kč",
            },
            { name: "Extra Light", price: "11,183.00 Kč" },
            { name: "Marathon", price: "10,553.00 Kč" },
            { name: "Kovová spojka", price: "+ 1,473.00 Kč" },
            { name: "obal", price: "+ 1,575.00 Kč" },
        ],
        sizes: [835,815,795,775,760,735,705,675],

        url:
            "https://paddles.braca-sport.com/paddles/kayak-flatwater/braca-xi-van-dusen-92.html",

        description:
            "Brača XI Van Dusen '92 is the newest addition to the Brača-Sport paddle line. It was originally designed by Ted Van Dusen in 1992 after three years of development and testing. Due to the high demand, Brača-Sport added this special shaped blade to the product line by purchasing a licence from Ted in 2014." +
            "\n" +
            "The blade has a special twisted tear-drop shape which facilitates a powerful catch and clean exit. It's movement through the water is different from other Brača-Sport blades, therefore it requires a slightly different paddling technique.",
    },

    {
        type: "canoe",
        name: "Brača Canoe Children",
        picture_name: "canoe_children.png",
        adjustment: "plastic",
        price: [
            {
                name: "Classic",
                price: "5,670,00 Kč",
            },

            { name: "Plastová spojka", price: "+ 1,073.00 Kč" },
            { name: "obal", price: "+ 1,575.00 Kč" },
        ],
        sizes: [16-18.5],
        url:
            "https://paddles.braca-sport.com/paddles/canoe-flatwater/braca-canoe-children.html",
        description:
            "Braca Canoe Children is přesná kopie Braca Canoe Unie 2000 s proporcionálně zmenšeným listem." +
            " Tvar a použité materiály umožňují absorbci nárazů každého záběru a redukuje rázy do ramen, loktů a zápěstí."+
            "  Pro dětská pádla máme výborné zkušenosti s plastovými spojkami. Jsou velmi pevné a lehčí. Také nejsou tak náchylné " +
            " na zarůstání jako hliníkové. Děti běžně zapomínají pádlo pravidelně rozebírat",
        description_eng:
            "Brača Canoe Children is an exact copy of the Brača Canoe Uni 2000 with proportionally smaller blade size. " +
            "The proprietary material characteristics and mechanics of the paddle are specifically designed to fit children's physiology.\n" +
            "\n" +
            "It enables absorption of impact from each paddle stroke, therefore reducing shock on the shoulders, elbows and wrists," +
            " ultimately decreasing common canoe injuries.\n" +
            "\n" +
            "You can also choose the stiffness of the shaft. We recommend a stiffer shaft for bigger blades but any combination is possible." +
            " See the shaft stiffness measurement prodecure and parameter table below.",
    },

    {
        type: "canoe",
        name: "Brača Canoe Uni Extra Wide",
        picture_name: "canoe_uni_extra_wide.png",
        adjustment: "metal",
        price: [
            {
                name: "Classic",
                price: "10,238.00 Kč",
            },
            { name: "Kovová spojka", price: "+ 1,473.00 Kč" },
            { name: "obal", price: "+ 1,575.00 Kč" },
        ],
        sizes: [],
        url:
            "https://paddles.braca-sport.com/paddles/canoe-flatwater/braca-canoe-uni-extra-wide.html",
        description:
            "is the line of Brača canoe sprint paddles designed and engineered to enable a very quick and powerful catch. This design facilitates application of all the power in the beginning of the stroke (catch) and an effortless exit.",
        description_eng:
            "is the line of Brača canoe sprint paddles designed and engineered to enable a very quick and powerful catch. This design facilitates application of all the power in the beginning of the stroke (catch) and an effortless exit.",
    },
];