export const boats_db =
    [
        {
            model: "K1 7",
            type: "kayak",

            sizes: ['S', 'M', 'L', 'XXL', 'XXXL'],
            picture: "k1-7.png",
            description: {
                CONCEPT:
                    "Vylepšete vítěznou loď tím, že zachováte dobré vlastnosti modelu Cinco a zároveň nabídnete lepší pocit z rychlosti a směru.",

                DESIGN:
                    "Konstrukce, která se déle drží blíže optimálnímu trimu a snižuje amplitudu náklonu a kurzu. Nový tvar měl také přispět k tuhosti lodě.",

                LAYUP:
                    "Vylepšená tuhost díky jednosměrnému karbonu umístěnému jako strukturální výztužná síť. Obruč vyztužená 100% karbonem zvyšuje tuhost konstrukční části lodi.",

                PERFORMANCE:
                    "Podobná maximální rychlost ve srovnání s modelem Cinco, ale s vylepšenou křivkou ztrát rychlosti, což má za následek efektivnější člun, který si déle udrží maximální rychlost. Menší pohyb směru a lepší stabilita umožňují silnější záběry a plynulejší klouzání.",

                SIZES:
                    "S modelem Sete pokrýváme širší váhový rozsah sportovců, od 55 kg do více než 100 kg. K1 a K2 Sete jsou nyní k dispozici ve velikostech S, M, ML, L, XXL a XXXL. K4 je k dispozici ve třech velikostech: M, ML a L.",

                FITTINGS:
                    "Nový systém řízení. Nová kormidelní lopatka ze 100% karbonu a nového tvaru. Nový T-Bar se snadným nastavením délky. Nové stahovací pásky kabelů. Vše dohromady pro efektivnější a plynulejší řešení řízení.",

                CUSTOMIZATION:
                    "Nemáte nápady pro svůj nový člun? Žádný problém! Kromě našich barevných schémat se můžete obrátit na Paddle Sports Design, našeho partnera pro špičkový osobní design. www.paddlesportsdesign.com."

            },
            url: 'http://www.nelo.eu/product/k1-7/?size=s'


        },
        {
            model: "K1 5",
            type: "kayak",

            sizes: ['S', 'M', 'L', 'XXL', 'XXXL'],
            picture: "k1-5.png",
            description:  {
                ABOUT:
                    "Obrácená příď trupu udržuje loď déle blíže optimálnímu trimu a omezuje nežádoucí změny směru. Díky obtékané palubě může sportovec provádět záběr a tlačení téměř bez povšimnutí, že tam paluba vůbec je, v novém pojetí naprosté svobody pohybu. Jeho na míru upravený kokpit poskytuje co nejpohodlnější polohu v každé ze 6 různých dostupných velikostí. Záď poskytuje obrovskou oporu v zadní části pro rychlejší efekt zotavení z vody, což snižuje náklon a pohyb směru. Nové kormidlo má nyní hlubší profil pro zvýšenou stabilizaci směru bez zvýšení odporu vzduchu nebo snížení rychlosti otáčení.",

                CUSTOMIZATION:
                    "Nemáte nápady pro svou novou loď? Žádný problém! Kromě našich barevných schémat se můžete obrátit na Paddle Sports Design, našeho partnera pro špičkový osobní design. www.paddlesportsdesign.com "

            },
            url: 'http://www.nelo.eu/product/k1-cinco/?size=s'


        },
        {
            model: "K1 4",
            type: "kayak",

            sizes: ['S', 'M', 'L', 'XXL', 'XXXL'],
            picture: "k1-4.png",
            description: {
                ABOUT:
                    "Nelo 4 je tradičně vyhlížející loď s velmi dobrou rovnováhou mezi pohybem a stabilitou. Nová čtyřka má několik vylepšení na přídi a na zádi, aby byla tato rovnováha ještě plynulejší.",

                CUSTOMIZATION:
                    "Nemáte nápad na svůj nový člun? Žádný problém! Kromě našich barevných schémat se můžete obrátit na Paddle Sports Design, našeho partnera pro špičkový osobní design. www.paddlesportsdesign.com."


            },


        }, {
        model: "K2 7",
        type: "kayak",

        sizes: ['S', 'M', 'ML', 'L', 'XXL', 'XXXL'],
        picture: "k2-7.png",
        description: {
            CONCEPT:
                "Vylepšete vítěznou loď tím, že zachováte dobré vlastnosti modelu Cinco a zároveň nabídnete lepší pocit z rychlosti a směru.",

            DESIGN:
                "Konstrukce, která se déle drží blíže optimálnímu trimu a snižuje amplitudu náklonu a kurzu. Nový tvar měl také přispět k tuhosti lodě.",

            LAYUP:
                "Vylepšená tuhost díky jednosměrnému karbonu umístěnému jako strukturální výztužná síť. Obruč vyztužená 100% karbonem zvyšuje tuhost konstrukční části lodi.",

            PERFORMANCE:
                "Podobná maximální rychlost ve srovnání s modelem Cinco, ale s vylepšenou křivkou ztrát rychlosti, což má za následek efektivnější člun, který si déle udrží maximální rychlost. Menší pohyb směru a lepší stabilita umožňují silnější záběry a plynulejší klouzání.",

            SIZES:
                "S modelem Sete pokrýváme širší váhový rozsah sportovců, od 55 kg do více než 100 kg. K1 a K2 Sete jsou nyní k dispozici ve velikostech S, M, ML, L, XXL a XXXL. K4 je k dispozici ve třech velikostech: M, ML a L.",

            FITTINGS:
                "Nový systém řízení. Nová kormidelní lopatka ze 100% karbonu a nového tvaru. Nový T-Bar se snadným nastavením délky. Nové stahovací pásky kabelů. Vše dohromady pro efektivnější a plynulejší řešení řízení.",

            CUSTOMIZATION:
                "Nemáte nápady pro svůj nový člun? Žádný problém! Kromě našich barevných schémat se můžete obrátit na Paddle Sports Design, našeho partnera pro špičkový osobní design. www.paddlesportsdesign.com."

        },
        url: 'http://www.nelo.eu/product/k2-7/?size=l'


    }, {
        model: "K4 7",
        type: "kayak",

        sizes: ['M', 'ML', 'L', 'XXL'],
        picture: "k4-7.png",
        description: {
            CONCEPT:
                "Vylepšete vítěznou loď tím, že zachováte dobré vlastnosti modelu Cinco a zároveň nabídnete lepší pocit z rychlosti a směru.",

            DESIGN:
                "Konstrukce, která se déle drží blíže optimálnímu trimu a snižuje amplitudu náklonu a kurzu. Nový tvar měl také přispět k tuhosti lodě.",

            LAYUP:
                "Vylepšená tuhost díky jednosměrnému karbonu umístěnému jako strukturální výztužná síť. Obruč vyztužená 100% karbonem zvyšuje tuhost konstrukční části lodi.",

            PERFORMANCE:
                "Podobná maximální rychlost ve srovnání s modelem Cinco, ale s vylepšenou křivkou ztrát rychlosti, což má za následek efektivnější člun, který si déle udrží maximální rychlost. Menší pohyb směru a lepší stabilita umožňují silnější záběry a plynulejší klouzání.",

            SIZES:
                "S modelem Sete pokrýváme širší váhový rozsah sportovců, od 55 kg do více než 100 kg. K1 a K2 Sete jsou nyní k dispozici ve velikostech S, M, ML, L, XXL a XXXL. K4 je k dispozici ve třech velikostech: M, ML a L.",

            FITTINGS:
                "Nový systém řízení. Nová kormidelní lopatka ze 100% karbonu a nového tvaru. Nový T-Bar se snadným nastavením délky. Nové stahovací pásky kabelů. Vše dohromady pro efektivnější a plynulejší řešení řízení.",

            CUSTOMIZATION:
                "Nemáte nápady pro svůj nový člun? Žádný problém! Kromě našich barevných schémat se můžete obrátit na Paddle Sports Design, našeho partnera pro špičkový osobní design. www.paddlesportsdesign.com."

        },
        url: 'http://www.nelo.eu/product/k4-7/?size=l'


    }, {
        model: "K4 5",
        type: "kayak",

        sizes: ['M', 'L'],
        picture: "k4-5.png",
        description: {
            ABOUT:
            "Obrácená příď trupu udržuje loď déle blíže optimálnímu trimu a omezuje nežádoucí změny směru. Díky obtékané palubě může sportovec provádět záběr a tlačení téměř bez povšimnutí, že tam paluba vůbec je, v novém pojetí naprosté svobody pohybu. Jeho na míru upravený kokpit poskytuje co nejpohodlnější polohu v každé z 5 různých dostupných velikostí. Záď poskytuje obrovskou oporu v zadní části pro rychlejší efekt zotavení z vody, což snižuje náklon a pohyb směru. Nové kormidlo má nyní hlubší profil pro zvýšenou stabilizaci směru bez zvýšení odporu vzduchu nebo snížení rychlosti otáčení.",

        },
        url: 'http://www.nelo.eu/product/k4-cinco/?size=l'


    },
        {
            model: "C1 8",
            type: "canoe",

            sizes: ['S', 'M', 'L', 'XL', 'XXL'],
            picture: "c1-8.png",
            description: {
                ABOUT:
                "C1 8 P24" ,
                CONCEPT: "Celá naše řada C1 byla aktualizována s výhledem na rok 2024." ,

                DESIGN: "V celé řadě byla provedena drobná, ale zásadní vylepšení, aby chování a pohyb lodi přesně odpovídaly tomu, co hledáme. Při vytváření koncepce lodi se snažíme o určitou toleranci náklonu, náklonu a pohybu, ale sportovci vzhledem ke svým technikám a specifikům často používají loď zcela odlišným způsobem. Použité změny mají za cíl přesně snížit počet sportovců a okamžiků, kdy loď není na této optimální úrovni." +

                "Hlavní změny se týkají rozložení objemu, což umožňuje pocit \"tlačení vpřed\" při šlápnutí na předek a rychlejší \"zotavení\" zádi po výjezdu z lopatky.",
            },
            url: 'http://www.nelo.eu/product/c1-8/?size=l'


        }, {
        model: "C2 8",
        type: "canoe",

        sizes: ['S', 'M', 'ML', 'L'],
        picture: "c2-8.png",
        description: {},
        url: 'http://www.nelo.eu/product/c2-8/?size=l'


    }, {
        model: "C4 8",
        type: "canoe",

        sizes: [],
        picture: "c4-8.png",
        description: {
            ABOUT: "Nová C4 8 je působivá loď nejen svými rozměry, ale i výkonem.",

            CONCEPT: "Zachovává si stejné vlastnosti jako model Nelo 8 C1:"+
            "Vytvoření rovnoběžných linií udržuje pohyb vyvážený a zároveň udržuje kánoi ve správném směru."+
                "Lepší vztlak při každém záběru, zvětšením rockeru ve střední části kanoe se snadno snižuje mokrý povrch kanoe během akcelerační fáze."+
            "Zlepšený směr díky vylepšené přídi a zádi, které vytvářejí méně turbulencí a udržují klouzání plynulejší a zároveň snadno korigují trajektorii."+

            "Kombinací těchto faktorů vznikl úžasný trup, který mohou sportovci využít k vyjádření svého plného potenciálu k dosažení maximálního klouzavého výkonu na záběr při plném pohodlí a stabilitě.",
        },
        url: 'http://www.nelo.eu/product/c4-8/'


    },

    ];