import React, {useState} from "react";
import * as Constants from "../../constants";
import {Col, Drawer, Layout, Row} from "antd";
import "./Boats.css";
import BoatDescription from "./BoatDescription";
import BoatDescriptionMobile     from "./BoatDescriptionMobile";
import {Link} from "react-router-dom";
import {boats_db} from "./boats_db";
import useWindowDimensions from "../hooks/getDimensionHook";
import {MenuOutlined} from "@ant-design/icons";


const Home = () => {
  const { Content } = Layout;
  const WrapperComponent = "a";

  const [selectedBoat, setSelectedBoat] = useState(boats_db[0]);
  const [openMenu, setOpenMenu] = useState(false)

  const Boat = (paddle) => {
    return (
        <a
            href={"#"}
            onClick={(e) => {
              e.preventDefault();
              setSelectedBoat(paddle);
            }}
        >
          {" "}
          {paddle.model}
        </a>
    );
  };

  const Boats = (boats_db, type) => {
    return (
        <ul>
          {boats_db.map((boat, index) => {
            if (boat.type === type) {
              return (
                  <li>
                    <Boat {...boat} />
                  </li>
              );
            }
          })}
        </ul>
    );
  };

  const LeftMenu = () => {
    return(
        <>
          Kanoe:
          {Boats(boats_db, "canoe")}
          Kajaky:
          {Boats(boats_db, "kayak")}

          Oblečení:
        </>
    );

  }


    const { height, width } = useWindowDimensions();
    let colSpan = (width < 800 ? 2 : 6);
    let colSpanBody = (width < 800 ? 22 : 18);
    console.debug("..............", height, width);

    return (
    <Content style={{ padding: "0 0px" }}>
      <div style={{ background: "#fff", padding: 24, minHeight: 500 }}>

          <Row>
              <Col span={colSpan}>
                  <div style={{backgroundColor: "darkblue", height: 35, paddingLeft: 9, width: 35, paddingTop: 9}}
                       onClick={() => {
                           setOpenMenu(true)
                       }}
                       className="menuIcon"
                  >
                      <MenuOutlined style={{color: "white", fontSize: "medium"}}/>
                  </div>
                  <span class="headerMenu">
                <LeftMenu/>
            </span>
                  {<Drawer open={openMenu}
                           closable={false}
                           placement="left"
                           onClose={() => {
                               setOpenMenu(false);
                           }}
                           bodyStyle={{backgroundColor: "darkblue"}}
                           width={290}

                  >
                      <LeftMenu />
                  </Drawer>}
              </Col>
              <Col span={colSpanBody}>
                  { (selectedBoat.type === 'kayak' || selectedBoat.type === 'canoe') && width < 800 &&  <BoatDescriptionMobile {...selectedBoat} />}
                  { (selectedBoat.type === 'kayak' || selectedBoat.type === 'canoe') && width > 800 &&  <BoatDescription {...selectedBoat} />}
                  {/*
                  { selectedPaddle.type === 'clothing' &&  <ClothingDescription {...selectedPaddle} />}
                  { selectedPaddle.type === 'accessories' &&  <AccessoriesDescription {...selectedPaddle} />}
                    */}

              </Col>
          </Row>
          <Row>
              <Col span={24}></Col>
          </Row>

          <Row>
              <Col span={24}>&nbsp;</Col>
          </Row>

          <Row>
              <Col key="1" span={12}>
                  kompletní nabídka v{" "}
                  <WrapperComponent
                      href={`http://nelo.eu`}
                      target="_empty"
                      title="braca-sport.com"
                  >
                      Nelo.eu
                  </WrapperComponent>
              </Col>
              <Col key={"2"} span={12}>
                  * Cenu lodí Vám zašleme na vyžádání
              </Col>
          </Row>



      </div>
    </Content>
  );
};
export default Home;





/*
puvodni obsah

<Row>
    <Col span={6}>
        <p>
            Od roku 2020 jsme výhradní dovozce lodí Nelo  do České
            Republiky.
            <br/>
            Nabízíme kompletni sortiment lodí a náhradních dílu firmy Nelo.
            <br/>
            Veškeré informace o produktech a cenách Vám poskytne náš obchodní zástupce.
        </p>
    </Col>
    <Col span={6}>
        <div style={{textAlign: 'left'}}>
            <div>
                <p style={{textAlign: 'left'}}>
                    <span>&nbsp;Petr Mokrý</span><br/>
                    <span>TNK Servis s.r.o</span><br/>
                    <span>U loděnice 966, 156 00 Praha 5 Zbraslav</span><br/>
                    <span>&nbsp;petr.mokry@kajak-kanoe.cz</span><br/>
                    <span>&nbsp;604 23 62 69</span><br/>
                </p>

            </div>
        </div>
    </Col>
    <Col span={12}>
        <img
            className="home_main_image"
            src={`${Constants.IMAGES_PATH}/nelo_logo.png`}
            alt="1X"
        />
    </Col>
</Row>    <Row>
    <Col span={6}>
        <p>
            <h4>Možnost tvorby vlastního designu na loď</h4>
        </p>
    </Col>
    <Col span={6}>
        <a href="https://paddlesportsdesign.com/shop-designs.html">
            PaddleSportDesign
        </a>


    </Col>
    <Col span={12}>
    </Col>
</Row>*/
