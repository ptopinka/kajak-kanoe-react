import React from "react";
import {Col, Layout, Row, Table, Modal} from "antd";
import * as Constants from "../../constants";
import styles from "./Boats.css";

const BoatDescriptionMobile = ({
                                   model,
                                   price,
                                   width,
                                   length,
                                   size,
                                   weight,
                                   construction,
                                   description,
                                   picture,
                                   url
                               }) => {
    const WrapperComponent = "a";

    const info = () => {
        Modal.info({
            width: 1000,
            title: '',
            content: (
                <div>
                    <img
                        class="image_boat_big_mobile"
                        src={`${Constants.IMAGES_PATH}/nelo/boats/big/${picture}`}
                        alt="1X"

                    />
                </div>
            ),
            onOk() {
            },
        });
    }


    const DescLine = ({title ,value}) => {
        return(
            <li><b>{title}:</b> <br/> {value}  </li>
        )
    }

    return (
        <div>
            <h2> {model}</h2>
            <span>
               <img
                   class="image_boat_mobile"
                   src={`${Constants.IMAGES_PATH}/nelo/boats/${picture}`}
                   alt="1X"
                   onClick={info}
               />
      </span>

            <div>
                <h3>Popis lodi</h3>
                <ul>
                    { description.ABOUT  && <DescLine title="O LODI" value={description.ABOUT} /> }
                    { description.CONCEPT  && <DescLine title="KONCEPT" value={description.CONCEPT} /> }
                    { description.DESIGN  && <DescLine title="DESIGN" value={description.DESIGN} /> }
                    { description.LAYUP  && <DescLine title="LAYUP" value={description.LAYUP} /> }
                    { description.PERFORMANCE  && <DescLine title="VÝKONNOST" value={description.PERFORMANCE} /> }
                    { description.SIZES  && <DescLine title="ROZMĚRY" value={description.SIZES} /> }
                    { description.FITTINGS  && <DescLine title="PŘÍSLUŠENSTVÍ" value={description.FITTINGS} /> }
                    { description.CUSTOMIZATION  && <DescLine title="PŘIZPŮSOBENÍ" value={description.CUSTOMIZATION} /> }


                </ul>
            </div>
            <div>
                <br/>
                <h3>kompletní popis lodi na stránkách výrobce {" "}</h3>
                <WrapperComponent
                    href={url}
                    target="_empty"
                    title={`nelo.eu/${model}`}
                >
                    {`nelo.eu/${model}`}
                </WrapperComponent>
            </div>
        </div>
    );
};


export default BoatDescriptionMobile;
