import React from "react";
import * as Constants from "../../constants";
import { useState } from "react";
import { Col, Layout, Row } from "antd";
import Header from "../Header";
import Footer from "../Footer";
import "./Parts.css";
import Description from "./Description";
const Parts = () => {
  const { Content } = Layout;
  const WrapperComponent = "a";
  const paddles = [
    {
      type: "kayak",
      name: "Knysna - Hubby foot pump",
      picture_name: "k2-footpump.jpg",
      price: [
        {
          name: "Classic",
          price: "1,815.00 Kč",
        },

      ],
      sizes: ["K1", "K2"],


      description:
        "Maratónská pumpa od Knysny. Nejpoužívanější pumpa ve maratónském světě. Spolehlivá, jednoduchá, funkční"
    },
    {
      type: "canoe",
      name: "Braca Canoe Záklek",
      picture_name: "canoe_knee_pad.png",
      price: [
        {
          name: "Classic",
          price: "990.00 Kč",
        },

      ],
      sizes: [""],


      description:
          "made from lightweight and durable closed mini-cell foam that can be shaped for a custom fit and superior comfort."
    },

  ];
  const [selectedPaddle, setSelectedPaddle] = useState(paddles[0]);

  const Paddle = (paddle) => {
    return (
      <a
        href={"#"}
        onClick={(e) => {
          e.preventDefault();
          setSelectedPaddle(paddle);
        }}
      >
        {" "}
        {paddle.name}
      </a>
    );
  };

  const Parts = (paddles, type) => {
    return (
      <ul>
        {paddles.map((paddle, index) => {
          if (paddle.type === type) {
            return (
              <li>
                <Paddle {...paddle} />
              </li>
            );
          }
        })}
      </ul>
    );
  };

  return (
    <Content style={{ padding: "0 0px" }}>
      <div style={{ background: "#fff", padding: 24, minHeight: 600 }}>
        <Row>
          <Col span={6}>
            Kajak:
            {Parts(paddles, "kayak")}
            Kanoe:
            {Parts(paddles, "canoe")}
          </Col>
          <Col span={18}>
            <Description {...selectedPaddle} />
          </Col>
        </Row>
        <Row>
          <Col span={24}></Col>
        </Row>

        <Row>
          <Col span={24}>&nbsp;</Col>
        </Row>

        <Row>
          <Col key="1" span={12}>
            kompletní nabídka v{" "}
            <WrapperComponent
              href={`https://paddles.braca-sport.com/paddles/kayak-flatwater/braca-i.html`}
              target="_empty"
              title="braca-sport.com"
            >
              Braca-Sport.com
            </WrapperComponent>
          </Col>
          <Col key={"2"} span={12}>
            * Cena zahrnuje DPH
          </Col>
        </Row>
      </div>
    </Content>
  );
};
export default Parts;
