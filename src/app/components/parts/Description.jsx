import React from "react";
import { Col, Layout, Row, Table, Modal } from "antd";
import * as Constants from "../../constants";
import "./Parts.css";

const Description = ({
  name,
  price,
 sizes,
  description,
  picture_name,
  url,
}) => {
  const WrapperComponent = "a";
  const columns = [
    {
      title: "Name",
      dataIndex: "name",
      key: "name",
    },
    {
      title: "Cena *",
      dataIndex: "price",
      key: "price",
    },
  ];
  const paginationProps = {
    hideOnSinglePage: true,
  };

  const info = () => {
    Modal.info({
      width: 1000,
      title: '',
      content: (
          <div>
            <img
                className="image_system"
                src={`${Constants.IMAGES_PATH}/parts/${picture_name}`}
                alt="1X"
            />
          </div>
      ),
      onOk() {},
    });
  }

  return (
    <Row gutter={[16, 16]}>
      <Col span={12} >
        <h2> {name}</h2>
        <div>
          <b>{name}</b>&nbsp;
          {description}
        </div>
        <Row><Col span={24}>&nbsp;</Col> </Row>
        <ul>
          <li>
            <span>velikosti: {sizes.join(", ")} cm&sup2;</span>
          </li>

          <li>
            <span>Cena </span>
            <Table
              columns={columns}
              dataSource={price}
              bordered
              pagination={{ ...paginationProps }}
            />
          </li>
        </ul>


      </Col>
      <Col span={12}>
        {" "}
        <img
          className="image_paddle"
          src={`${Constants.IMAGES_PATH}/parts/${picture_name}`}
          alt="1X"
          onClick={info}
        />
        <br/>
        <br/>
        <br/>

      </Col>
    </Row>
  );
};
export default Description;
