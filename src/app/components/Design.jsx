import React from "react";
import { Layout } from "antd";
import Header from './Header';
import Footer from "./Footer";
const Design = () => {
    const {  Content } = Layout;
    return(
        <>
            <Header/>

            <Content style={{ padding: "0 50px" }}>

                <div style={{ background: "#fff", padding: 0, minHeight: 280 }}>
                    DESIGN
                </div>
            </Content>
            <Footer/>

        </>
    );
};
export default Design;
