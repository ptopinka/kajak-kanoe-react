import React from "react";
import { HashRouter, Route, Switch } from "react-router-dom";
import Design from "./components/Design";
import Paddles from "./components/paddles/Paddles";
import Contacts from "./components/contacts/Contacts";
import Technology from "./components/technology/Technology";
import Media from "./components/media/Media";
import Footer from "./components/Footer";
import Header from "./components/Header";
import Boats from "./components/boats/Boats";
import Parts from "./components/parts/Parts";
import Home from "./components/home/Home";

const Module = ({ children }) => {
  console.debug("test");
  return (
    <>
      <Header />
      {children}
      <Footer />
    </>
  );
};

const Router = () => {
  return (
    <HashRouter>
      <Switch>
        <Route path="/paddles">
          <Module>
            <Paddles />
          </Module>
        </Route>
        <Route path="/boats">
          <Module>
            <Boats />
          </Module>
        </Route>
        <Route path="/parts">
          <Module>
            <Parts />
          </Module>
        </Route>
        <Route path="/media">
          <Module>
            <Media />
          </Module>
        </Route>
        <Route path="/contact">
          <Module>
            <Contacts />
          </Module>
        </Route>
        <Route path="/">
          <Module>
            <Home />
          </Module>
        </Route>
      </Switch>
    </HashRouter>
  );
};

export default Router;
