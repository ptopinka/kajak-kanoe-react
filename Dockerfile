FROM node:15-alpine

EXPOSE 3000

RUN apk add --update tini

WORKDIR /usr/src/app/

COPY package.json package.json

RUN yarn install

COPY . .

CMD ["tini", "--", "yarn", "start"]